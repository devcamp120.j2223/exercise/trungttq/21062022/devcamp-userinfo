import {gUserInfo} from "./info"
 

function App() {
  return (
    <div>
      <h5>{gUserInfo.firstname} {gUserInfo.lastname}</h5>
      <img src={gUserInfo.avatar} width="300px"/>
      <p>{gUserInfo.age}</p>
      <p>{(gUserInfo.age <= 35)? "anh ấy còn trẻ" : "anh ấy đã già"}</p>
      <ul>
        {
          gUserInfo.language.map((element, index) => {
            return <li key={index}>{element}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
