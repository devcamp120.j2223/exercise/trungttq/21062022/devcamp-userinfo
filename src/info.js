import avatar from "./assets/images/avatardefault_92824.webp"
export const gUserInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: avatar,
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
  }